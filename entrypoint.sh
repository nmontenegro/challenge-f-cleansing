#!/bin/bash

cd /app

mkdir ssh_key
echo $KNOWN_HOSTS | base64 -d > ./ssh_key/known_hosts
echo $SSH_KEY | base64 -d > ./ssh_key/sftp-server

exec java -jar cleansing-0.0.1-SNAPSHOT.jar
