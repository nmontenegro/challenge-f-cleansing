FROM maven:3.6.3-jdk-11 AS builder
WORKDIR /code
COPY ./pom.xml /code/pom.xml
COPY ./.mvn /code/.mvn
RUN mvn dependency:resolve
COPY ./src /code/src
RUN mvn package -PdockerBuildDir

FROM mcr.microsoft.com/java/jre:11u7-zulu-alpine AS runtime
WORKDIR /app
COPY --from=builder /code/output /app
COPY ./entrypoint.sh /app

ENTRYPOINT ["sh", "/app/entrypoint.sh"]
