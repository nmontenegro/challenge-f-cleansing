package com.chanllengef.cleansing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.apache.commons.codec.digest.DigestUtils;

public class Cleaner {
    public static InputStream cleaner(InputStream csv_file, String col_sanitize) throws IOException {
        CSVReader reader = new CSVReader(new InputStreamReader(csv_file));
        List<String[]> csvBody = reader.readAll();
        reader.close();

        int num_col_sanitize =  Arrays.asList(csvBody.get(0)).indexOf(col_sanitize);
        
        for (int i = 0; i < csvBody.size(); i++) {
            if (i == 0){
                continue;
            } else {
                csvBody.get(i)[num_col_sanitize] = DigestUtils.md5Hex(csvBody.get(i)[num_col_sanitize]).toUpperCase();
            }
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(out));
        csvWriter.writeAll(csvBody);
        csvWriter.flush();
        csvWriter.close();
        InputStream out_csv_file = new ByteArrayInputStream(out.toByteArray());
        
        return out_csv_file;
    }
}