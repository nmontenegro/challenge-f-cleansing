package com.chanllengef.cleansing;

import com.jcraft.jsch.ChannelSftp;

import org.apache.commons.codec.binary.Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@SpringBootApplication
@RestController
public class CleansingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleansingApplication.class, args);
	}

	@GetMapping("/")
	public String hello() {
		return "OK";
	}

	@PostMapping(value = "/cleansing", consumes = "application/json", produces = "application/json")
	public String employee(@RequestBody MessageWrapper message, ChannelSftp channelSftp) {
		try {
			String datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

			InputStream stream = new ByteArrayInputStream(
					(message.getMessage().getData() + "\n" + message.getMessage().getAttributes())
							.getBytes(StandardCharsets.UTF_8));

			String filename = new String(Base64.decodeBase64(message.getMessage().getData().getBytes()));
			InputStream csv_file = DownloadObject.downloadObject("clean-truth-279701", "cleansing-input", filename);

			InputStream csv_file_clean = Cleaner.cleaner(csv_file, "employee_name");

			channelSftp = setupJsch();
			channelSftp.connect();

			String remoteDir = "/home/nmontenegro.challenges/remote_sftp_test/";
			channelSftp.put(stream, remoteDir + "input_pubsub" + datetime + ".txt");
			channelSftp.put(csv_file_clean, remoteDir + "input_pubsub" + datetime + ".csv");

			channelSftp.exit();
		} catch (JSchException | SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "{\"result\": true}";
	}


	private ChannelSftp setupJsch() throws JSchException {
		final JSch jsch = new JSch();
		jsch.setKnownHosts("./ssh_key/known_hosts");
		jsch.addIdentity("./ssh_key/sftp-server");
		final Session jschSession = jsch.getSession("nmontenegro.challenges", "104.198.198.103");
		jschSession.connect();
		return (ChannelSftp) jschSession.openChannel("sftp");
	}
}
