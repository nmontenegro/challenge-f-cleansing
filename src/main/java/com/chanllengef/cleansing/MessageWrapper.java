package com.chanllengef.cleansing;

public class MessageWrapper {

    private MessageSub message;
    private String subscription;

    public MessageWrapper () {

    }

    public MessageSub getMessage() {
        return message;
    }

    public void setMessage(MessageSub message) {
        this.message = message;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    
}